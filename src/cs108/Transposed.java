package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class Transposed implements TextImage {
    private final TextImage shape;

    public Transposed(TextImage shape) {
        this.shape = shape;
    }

    @Override
    public int width() {
        return shape.height();
    }

    @Override
    public int height() {
        return shape.width();
    }

    @Override
    public List<String> drawing() {
    	List<StringBuilder> bs = new ArrayList<>(height());
		for (int i = 0; i < height(); ++i)
		    bs.add(new StringBuilder());
		for (String l: shape.drawing()) {
		    for (int i = 0; i < l.length(); ++i)
		        bs.get(i).append(l.charAt(i));
		}
		List<String> d = new ArrayList<>(height());
		for (StringBuilder b: bs)
		    d.add(b.toString());
		return Collections.unmodifiableList(d);
    }
}
