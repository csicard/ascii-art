package cs108;

import java.util.Collections;
import java.util.List;

final class Filled implements TextImage {
    private final int width, height;
    private final char fill;

    public Filled(int width, int height, char fill) {
        if (! (width >= 0))
            throw new IllegalArgumentException();
        if (! (height >= 0))
            throw new IllegalArgumentException();

        this.width = width;
        this.height = height;
        this.fill = fill;
    }

    @Override
    public int width() {
        return width;
    }

    @Override
    public int height() {
        return height;
    }

    @Override
    public List<String> drawing() {
        return Collections.nCopies(height, Strings.nCopies(width, fill));
    }
}
