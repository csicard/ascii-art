package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class FlippedHorizontally implements TextImage {
    private final TextImage shape;

    public FlippedHorizontally(TextImage shape) {
        this.shape = shape;
    }

    @Override
    public int width() {
        return shape.width();
    }

    @Override
    public int height() {
        return shape.height();
    }

    @Override
    public List<String> drawing() {
        List<String> o = shape.drawing();
        List<String> f = new ArrayList<>(o.size());
        for (String l: o)
            f.add(Strings.reverse(l));
        return Collections.unmodifiableList(f);
    }
}
