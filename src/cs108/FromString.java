package cs108;

import java.util.List;

final class FromString implements TextImage {
    private final String string;

    public FromString(String string) {
        this.string = string;
    }

    @Override
    public int width() {
        return string.length();
    }

    @Override
    public int height() {
        return 1;
    }

    @Override
    public List<String> drawing() {
        return List.of(string);
    }
}
