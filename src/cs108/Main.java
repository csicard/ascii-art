package cs108;

public class Main {
    private final static int CELL_WIDTH = 3;
    private final static int CELL_HEIGHT = 2;

    public static void main(String[] args) {
        // Dessine un échiquier.
        TextImage black = TextImage.filled(CELL_WIDTH, CELL_HEIGHT, '#');
        TextImage white = TextImage.filled(CELL_WIDTH, CELL_HEIGHT, ' ');
        TextImage bw = black.leftOf(white);
        TextImage wb = bw.flippedHorizontally();
        TextImage board2 = bw.above(wb);
        TextImage board4 = (board2.leftOf(board2)).above(board2.leftOf(board2));
        TextImage board8 = (board4.leftOf(board4)).above(board4.leftOf(board4));
        board8.framed().printOn(System.out);
    }
}
