package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class Above implements TextImage {
    private final TextImage top, bottom;

    public Above(TextImage top, TextImage bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    @Override
    public int width() {
        return Math.max(top.width(), bottom.width());
    }

    @Override
    public int height() {
        return top.height() + bottom.height();
    }

    @Override
    public List<String> drawing() {
        List<String> d = new ArrayList<>(height());
        int w = width();
        for (String l: top.drawing())
            d.add(l + Strings.nCopies(w - top.width(), ' '));
        for (String l: bottom.drawing())
            d.add(l + Strings.nCopies(w - bottom.width(), ' '));
        return Collections.unmodifiableList(d);
    }
}
