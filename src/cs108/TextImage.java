package cs108;

import java.io.PrintStream;
import java.util.List;

public interface TextImage {
    public int width();
    public int height();
    public List<String> drawing();

    public static TextImage fromString(String s) {
        return new FromString(s);
    }

    public static TextImage filled(int width, int height, char fill) {
        return new Filled(width, height, fill);
    }

    default public TextImage flippedHorizontally() {
        return new FlippedHorizontally(this);
    }

    default public TextImage transposed() {
        return new Transposed(this);
    }

    default public TextImage framed() {
        TextImage tb = fromString('+' + Strings.nCopies(width(), '-') + '+');
        TextImage lr = fromString(Strings.nCopies(height(), '|')).transposed();
        return tb.above(lr.leftOf(this.leftOf(lr)).above(tb));
    }

    default public TextImage leftOf(TextImage that) {
        return new LeftOf(this, that);
    }

    default public TextImage above(TextImage that) {
        return new Above(this, that);
    }

    default public void printOn(PrintStream s) {
        drawing().forEach(s::println);
    }
}
