package cs108;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class LeftOf implements TextImage {
    private final TextImage left, right;

    public LeftOf(TextImage left, TextImage right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int width() {
        return left.width() + right.width();
    }

    @Override
    public int height() {
        return Math.max(left.height(), right.height());
    }

    @Override
    public List<String> drawing() {
        List<String> d = new ArrayList<>(height());
        Iterator<String> lI = left.drawing().iterator();
        Iterator<String> rI = right.drawing().iterator();
        while (lI.hasNext() || rI.hasNext()) {
            String l = lI.hasNext() ? lI.next() : Strings.nCopies(left.width(), ' ');
            String r = rI.hasNext() ? rI.next() : Strings.nCopies(right.width(), ' ');
            d.add(l + r);
        }
        return Collections.unmodifiableList(d);
    }
}
